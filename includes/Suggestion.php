<?php

/**
 * A class to handle title suggestions.
 *
 * Copyright (C) 2019  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger <mah@everybody.org>
 */
namespace MediaWiki\Extension\SuggestedTitles;

use ContextSource;
use Sanitizer;
use Title;

class Suggestion extends ContextSource {
	protected static $instance = null;
	protected $titleCache = array();

	public static function instance() {
		if ( self::$instance === null ) {
			$class = get_called_class();
			self::$instance = new $class;
		}
		return self::$instance;
	}

	/**
	 * Get the only suggested replacement for this title.  If there are more
	 * than one suggestion or none at all, return false.
	 *
	 * @param Title $title
	 * @return false|string
	 */
	public function getSuggestion( Title $title ) {
		$suggestion = $this->getAllSuggestions( $title );
		if ( count( $suggestion ) === 1 ) {
			return $suggestion[0];
		}
		return false;
	}

	/**
	 * Get all suggested alternatives for this title.
	 *
	 * @param Title $title
	 * @return array
	 */
	public function getAllSuggestions( Title $title ) {
		return $this->doLookup( $title );
	}

	/**
	 * Boolean check for any suggestions.
	 *
	 * @param Title $title
	 * @return bool
	 */
	public function hasSuggestions( Title $title ) {
		$suggestion = $this->getAllSuggestions( $title );
		if ( $suggestion !== false && count( $suggestion ) >= 1 ) {
			return true;
		}
		return false;
	}

	/**
	 * Should this title redirect to a suggestion?
	 *
	 * @param Title $title
	 * @return bool
	 */
	public function shouldRedirect( Title $title ) {
		$req = $this->getRequest();

		// Skip all this if they're trying to edit a page
		if ( $req->getVal( "action", "read" ) === "edit" || $title->getNamespace() < 0 ) {
			return false;
		}

		$suggestion = $this->getSuggestion( $title );
		if ( $suggestion !== false && $req->getVal( "redirect", "yes" ) !== "no" ) {
			return true;
		}
		return false;
	}

	/**
	 * If this title should be redirected to a suggestion, handle that and
	 * return true telling caller that the user is being redirected.
	 *
	 * @param Title $title
	 * @return bool
	 */
	public function maybeRedirect( Title $title ) {
		static $inside = false;

		if ( $this->shouldRedirect( $title ) && $inside === false ) {
			$inside = true;
			$suggestion = $this->getSuggestion( $title );

			$title = Title::newFromText( $suggestion );
			$title->mRedirect = true;
			$exists = true;
			$this->getOutput()->mRedirect = $title->getFullURL();
			$inside = false;
			return true;
		}
		return false;
	}

	/**
	 * Given a title, return an array of suggested alternatives.
	 *
	 * @param Title $title
	 * @return string[]
	 */
	protected function getAlternatives( Title $title ) {
		$list = $this->doLookup( $title );
		$ret  = array();

		if ( $list !== false ) {
			// another place where this is wrong.
			$titleKey = $this->getTitleKey( $title );
			$ret = array_diff( $list, array( $titleKey ) );
		}
		return $ret;
	}

	/**
	 * Return a standard lookup key based on the title.
	 *
	 * @param Title|string $title
	 * @param ?int|null $ns
	 * @return string
	 */
	protected function getTitleKey( $title, $ns = null ) {
		$titleKey = null;
		if ( is_string( $title ) && is_int( $ns ) ) {
			// Taken from inside Title::newFromTextThrow()
			$title = strtr(
				Sanitizer::decodeCharReferencesAndNormalize( $title ), ' ', '_'
			);
			$titleKey = "$ns:$title";
		} elseif ( !is_string( $title ) ) {
			$titleKey = $title->getNamespace() . ':' . $title->getDBkey();
		}
		return $titleKey;
	}

	/**
	 * Return a list of suggestions for a title.
	 *
	 * @param Title $title
	 * @return string[]
	 */
	protected function doLookup( Title $title ) {
		$titleKey = $this->getTitleKey( $title );
		$titleNS  = $title->getNamespace();
		$titleUP  = mb_strtoupper( $title->getDBkey(), 'UTF-8' );

		if ( $titleNS < 0 ) {
			return array();
		}

		if ( !isset( $this->titleCache[$titleKey] ) ) {
			$dbr = wfGetDB( defined( 'DB_SLAVE' ) ? DB_SLAVE : DB_REPLICA );
			$ret = [];
			$res = $dbr->select(
				'page', 'page_title', [
					'page_namespace' => $titleNS,
					'UPPER( CONVERT( page_title USING utf8 ) )' => $titleUP
					# We may want this later.
					#'page_is_redirect' => 0
				], __METHOD__
			);
			$alts = array();
			foreach ( $res as $val ) {
				$ret[] = Title::makeName( $titleNS, $val->page_title );
				$alts[] = $this->getTitleKey( $val->page_title );
			}
			$this->titleCache[$titleKey] = array();
			if ( count( $ret ) > 0 ) {
				$this->titleCache[$titleKey] = $ret;
				foreach ( $alts as $val ) {
					$this->titleCache[$val] = $ret;
				}
			}
		}
		return $this->titleCache[$titleKey];
	}

	/**
	 * Given a URL and a Title object, alter the URL and title to point to
	 * the page that the user would be redirected to, if any.
	 *
	 * @param Title $title
	 * @param string $url
	 * @return string
	 */
	public function getLocalURL( Title $title, $url ) {
		$oldPartial = $title->getPrefixedDBkey();
		$suggest = $this->getSuggestion( $title );

		if ( $suggest !== false ) {
			// Title is returned this way
			$title = Title::newFromText( $suggest );
			return str_replace( $oldPartial, $suggest, $url );
		}
		return $url;
	}
}
