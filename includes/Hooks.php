<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger <mah@everybody.org>
 */
namespace MediaWiki\Extension\SuggestedTitles;

use Article;
use Title;
use Xml;

class Hooks {
	/**
	 * @see   https://www.mediawiki.org/wiki/Manual:Hooks/ShowMissingArticle
	 * @param Article $article
	 * @return void
	 */
	public static function onShowMissingArticle( Article $article ) {
		//throw new Exception( __METHOD__ );
		// Second place seen
	}

	/**
	 * @see    https://www.mediawiki.org/wiki/Manual:Hooks/BeforeDisplayNoArticleText
	 * @param Article $article
	 * @return bool false to skip error message
	 */
	public static function onBeforeDisplayNoArticleText( Article $article ) {
		$suggest = Suggestion::instance();
		$title   = $article->getTitle();

		if ( $suggest->hasSuggestions( $title ) ) {
			$text = false;
			if ( !$suggest->maybeRedirect( $title ) ) {
				$text = wfMessage(
					'suggestedtitles-redirect', $suggest->getSuggestion( $title ), $title
				)->plain();
			} elseif ( !$suggest->shouldRedirect( $title ) ) {
				$text = wfMessage(
					'suggestedtitles-alternatives',
					"\n* " . implode(
						"\n* ", array_map(
							function ( $title ) {
								return '[[' . $title . ']]';
							}, $suggest->getAllSuggestions( $title )
						)
					)
				)->plain();
			}

			# If we aren't re-directing, we'll have text to display:
			if ( $text !== false ) {
				$lang = $suggest->getLanguage();
				$dir = $lang->getDir();
				$suggest->getOutput()->addWikiText(
					Xml::openElement(
						'div', array(
							'class' => "noarticletext mw-content-$dir",
							'dir' => $dir,
							'lang' => $lang->getCode()
						)
					) . "\n$text\n</div>"
				);
			}
			return false;
		}
	}

	/**
	 * Always pointing to the right URL
	 *
	 * @see   https://www.mediawiki.org/wiki/Manual:Hooks/GetLocalURL
	 * @param Title $title
	 * @param string &$url
	 * @param string $query
	 * @return void
	 */
	public static function onGetLocalURL( Title $title, &$url, $query ) {
		$suggest = Suggestion::instance();
		$url = $suggest->getLocalURL( $title, $url );
	}

	/**
	 * Don't redlink if we've changed the link to a working page
	 *
	 * @see   https://www.mediawiki.org/wiki/Manual:Hooks/TitleIsAlwaysKnown
	 * @param Title $title
	 * @param bool|null &$isKnown (bool will cause Title::IsAlwaysKnown to
	 *                            return right after this is executed.)
	 * @return void
	 */
	public static function onTitleIsAlwaysKnown( Title $title, &$isKnown ) {
		$suggest = Suggestion::instance();
		if ( $suggest->getSuggestion( $title ) !== false ) {
			$isKnown = true;
		}
	}
}
